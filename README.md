# frontend-asv

El frontend de la app está desarrollado en **Angular**, para poder ejecutarlo en solitario necesita lo siguiente:
* Node
* npm
* angular

## Como instalarlo

Clona el repositorio

```
git clone https://antonioalonso2001@bitbucket.org/antonioalonso2001/frontend-asv.git
```

Entra a la carpeta **frontend-asv** recién creada y ejecuta:

```
npm install
ng build --prod
```
