export const environment = {
  production: true,
  teamsApiUrl: 'http://localhost:8080/api/teams',
  countriesApiUrl: 'http://localhost:8080/api/countries'   
};
