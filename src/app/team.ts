import { Country } from './country';
import { Competition } from './competitions';

export class Team{
    id: number;
    name: string;
    image: string;
    competitions_count: number;
    country: Country;
    rival?: Team;
    competitions: Competition[];
    country_id:string;
    rival_id?:number;
}