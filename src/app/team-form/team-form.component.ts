import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


import { TeamService }  from '../team.service';
import { Team } from '../team'
import { Country } from '../country';
import { Sort } from '../sort';

@Component({
  selector: 'app-team-form',
  templateUrl: './team-form.component.html',
  styleUrls: ['./team-form.component.css']
})
export class TeamFormComponent implements OnInit {

  team: Team;
  title: string;

  private formCreate = true;

  countries:Country[] = [];

  rivals:Team[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,    
    private TeamService: TeamService
  ) { }

  ngOnInit() {
    this.getCountries();
    this.getRivals();

    this.route.params.subscribe(params => {
      const id = +params['id'];
      if (id) {
        this.getTeam(id);
        this.formCreate = false;
        this.title = 'Editar Equipo';
      } else {
        this.team = new Team();
        this.title = 'Añadir equipo';
      }
    });    
  }

  onSubmit(): void {
    this.formCreate
      ? this.createTeam()
      : this.updateTeam();
  }

  createTeam(){
    this.TeamService.createTeam(this.team)
      .subscribe(
        team => this.router.navigate(['detail/'+team.id])
      );
  }

  updateTeam(){
    this.TeamService.updateTeam(this.team)
      .subscribe(
        team => this.router.navigate(['detail/'+team.id])
      );
  }

  getTeam(id:number): void {
    this.TeamService.getTeam(id)
      .subscribe(team => {
        this.team = team
      });
  }

  getCountries():void{
    this.TeamService.getCountries()
    .subscribe(countries => this.countries = countries);
  }

  getRivals():void{
    let sort = new Sort();

    sort.sort = 'name';
    sort.order = 'asc';

    this.TeamService.getTeams(sort)
    .subscribe(rivals => this.rivals = rivals);
  }

}
