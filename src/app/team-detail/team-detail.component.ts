import { Component, OnInit,Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { TeamService }  from '../team.service';
import { Team } from '../team'
import { Competition } from '../competitions';


@Component({
  selector: 'app-team-detail',
  templateUrl: './team-detail.component.html',
  styleUrls: ['./team-detail.component.css']
})
export class TeamDetailComponent implements OnInit {

  @Input() team: Team;
  date: Date;

  constructor(
    private route: ActivatedRoute,
    private TeamService: TeamService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getTeam();
  }

  addCompetition(): void{
    if(this.date)
    {
      let competition = new Competition;

      competition.date=this.date;
  
      this.date=null;
      this.team.competitions.push(competition);
      this.team.competitions_count ++;
  
      const id = +this.route.snapshot.paramMap.get('id');
      this.TeamService.addCompetition(id,competition)
      .subscribe();
    }
  }

  getTeam(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.TeamService.getTeam(id)
      .subscribe(team => this.team = team);
  }

  goBack(): void {
    this.location.back();
  }
}
