import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TeamListComponent }   from './team-list/team-list.component';
import { TeamDetailComponent }   from './team-detail/team-detail.component';
import { TeamFormComponent } from './team-form/team-form.component';


const routes: Routes = [
  { path: '', redirectTo: '/list', pathMatch: 'full' },
  { path: 'list', component: TeamListComponent },
  { path: 'detail/:id', component: TeamDetailComponent },
  { path: 'edit/:id', component: TeamFormComponent },
  { path: 'create', component: TeamFormComponent },
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
