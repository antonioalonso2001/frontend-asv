import { Component, OnInit } from '@angular/core';
import { TeamService } from '../team.service';
import { Team } from '../team';
import { Sort } from '../sort';


@Component({
  selector: 'app-team',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit {
  teams: Team[];
  sortMe: Sort = new Sort();

  constructor(private TeamService: TeamService) { }

  getTeams():void {
    this.TeamService.getTeams(this.sortMe)
      .subscribe(teams => this.teams = teams);
  }

  deleteTeam(team:Team): void {
    this.teams = this.teams.filter(t => t !== team);
    this.TeamService.deleteTeam(team.id).subscribe();
  }

  ngOnInit() {
    this.sortMe.sort='name';
    this.sortMe.order='asc';
    this.getTeams();
  }
}
