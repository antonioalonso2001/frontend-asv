import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse,HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Team } from './team';
import { Country } from './country';
import { Competition } from './competitions';
import { Sort } from './sort';
import { environment } from '../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class TeamService {
  private teamsUrl = environment.teamsApiUrl;
  private countriesUrl = environment.countriesApiUrl;

  constructor(
    private http: HttpClient
  ) { }

  getTeams(sort:Sort): Observable<Team[]>{
    let params = "sort="+sort.sort+"&order="+sort.order;
    
    return this.http.get<Team[]>(this.teamsUrl+"?"+params)
    .pipe(
      catchError(this.handleError)
    );
  }

  createTeam(team:Team): Observable<Team>{
    return this.http.post<Team>(this.teamsUrl,this.toFormData(team))
    .pipe(
      catchError(this.handleError)
    );    
  }

  updateTeam(team:Team): Observable<Team>{
    return this.http.post<Team>(this.teamsUrl+'/'+team.id,this.toFormData(team))
    .pipe(
      catchError(this.handleError)
    );
  }

  getCountries(): Observable<Country[]>{
    return this.http.get<Country[]>(this.countriesUrl)
    .pipe(
      catchError(this.handleError)
    );
  }

  getTeam(id:number): Observable<Team> {
    return this.http.get<Team>(this.teamsUrl+'/'+id)
    .pipe(
      catchError(this.handleError)
    );
  }

  deleteTeam(id:number): Observable<Team> {
    return this.http.delete<Team>(this.teamsUrl+'/'+id)
    .pipe(
      catchError(this.handleError)
    );
  }

  addCompetition(id:number ,competition:Competition){
    return this.http.post(this.teamsUrl+'/'+id+'/competitions',this.toFormData(competition))
    .pipe(
      catchError(this.handleError)
    );    
  }

  private toFormData(data: Object): FormData{
    let formData = new FormData();

    for ( var key in data ) {
      formData.append(key, data[key]);
    }

    return formData;
  }


  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      console.log(error);
      // The response body may contain clues as to what went wrong,

    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };
}
